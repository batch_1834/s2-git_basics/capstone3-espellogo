const productsData = [
	{
		id: "frd001",
		name: "Nike",
		description: "The world's largest athletic apparel company, Nike is best known for its footwear, apparel, and equipment. Founded in 1964 as Blue Ribbon Sports, the company became Nike in 1971 after the Greek goddess of victory. One of the most valuable brands among sport businesses, Nike employs over 76,000 people worldwide.",
		price: 9999 ,
		isActive: true
	},
	{
		id: "tyt002",
		name: "Addidas",
		description: "Adidas AG (adidas) designs, manufactures and markets athletic and sports lifestyle products. The company's product portfolio includes footwear, apparel and accessories such as bags, sun glasses, fitness equipment, and balls.",
		price: 7800,
		isActive: true
	},
	{
		id: "mtbs003",
		name: "VF Corp",
		description: "VF Corporation (formerly Vanity Fair Mills until 1969) is an American global apparel and footwear company founded in 1899 and headquartered in Denver, Colorado. The company's 13 brands are organized into three categories: Outdoor, Active and Work.",
		price: 6500 ,
		isActive: true
	}
]