import {useState, useEffect} from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";

import {UserProvider} from "./UserContext";

import AppNavbar from "./components/AppNavbar";

import Home from "./pages/Home";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import AdminDashboard from "./pages/AdminDashboard";
import AdminViewProducts from "./pages/AdminViewProducts";
import Cart from "./pages/Cart";
import Error from "./pages/Error";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";



import {Container} from "react-bootstrap";
import './App.css';



function App() {

const [user, setUser] = useState({

    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
    
  })
  console.log(user)

const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(()=>{
    console.log(user)
    console.log(localStorage);
  }, [user])

  // To update the User State upon page load if a user already exist.
  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
    })
    .then(res => res.json())
    .then(data =>{
      console.log(data);

      if (typeof data._id !== "undefined") {

        // this will be set to the user state.
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
        // set back the initial state of the user.
        setUser({
          id: null,
          isAdmin: null
        })
      }

      
    })
  },[])





    return (

      <UserProvider value={{user, setUser, unsetUser}} >
        <Router>

        <AppNavbar/>
          <Container fluid>
              {/*Routes holds all our Route components.*/}
              <Routes>
              {/*
                -Route assigns an endpoint and displays the appropiate page component for that endpoit.
                - "path" attribute assigns the endpoint.
                - "element" attribute assigns page component to be displayed at the endpoint.
              */}
                <Route exact path = "/" element={<Home/>} /> 

                <Route exact path = "/products" element={<Products/>} />
                <Route exact path = "/admin" element={<AdminDashboard/>} />
                <Route exact path = "/cart" element={<AdminDashboard/>} />
                <Route exact path = "/productsView/:productId" element={<AdminViewProducts/>} />
                <Route exact path = "/products/:productId" element={<ProductView/>}/>
                <Route exact path = "/register" element={<Register/>} />
                <Route exact path = "/login" element={<Login/>} /> 
                <Route exact path = "/logout" element={<Logout/>} />
                <Route exact path = "*" element={<Error/>} />
              </Routes>
          </Container>
        </Router>
      </UserProvider>
    );
  }



export default App;
