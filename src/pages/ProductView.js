import {useState, useEffect, useContext} from "react";
import {useParams, useNavigate} from "react-router-dom";
import Swal from "sweetalert2";
import {Container, Card, Button, Row, Col} from "react-bootstrap";

import UserContext from "../UserContext";


export default function ProductView(){

	// to check if their is already a logged in user. Change the button from "Enroll" to "Login" if the user is not logged in.
	const {user} = useContext(UserContext);

	const navigate = useNavigate();
	// "useParams" hook allows us to retrieve the courseId passed via URL.
	const {productId} = useParams();

	// Create state hooks to capture the information of a specific course and display it in our application.
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("")
	const [prodPrice, setProdPrice] = useState(0)
	const [isActive, setIsActive] = useState(false)
	

	const buy = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/buy`,{
			method: "POST",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
		
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {
				Swal.fire({
					title: "Succesfully Added!",
					icon: "success",
					text: `Thank you! Successfully added to cart!`
				})
				navigate("/products")
			}
			else{
				Swal.fire({
					title: "Please Register/Login before Buying!",
					icon: "error",
					text: "Thank you!"
				})
				navigate("/login")
			}

		})
	}

	useEffect(()=>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/`)
		.then(res => res.json())
		.then(data =>{



			console.log(data);

			setProductName(data.productName);
			setDescription(data.description);
			setProdPrice(data.prodPrice);
			setIsActive(data.isActive);
				
		})

	},[productId])



	return(
		<Container className="mt-5 d-flex-row justify-content-center">
					<Row>
						<Col sm={{ span: 6, offset: 3 }}>
							<Card>
								<Card.Body className="text-center">
									<Card.Title>{productName}</Card.Title>
									<Card.Subtitle>Description:</Card.Subtitle>
									<Card.Text>{description}</Card.Text>
									<Card.Subtitle>Price:</Card.Subtitle>
									<Card.Text>PhP {prodPrice}</Card.Text>
									<Card.Text>						
										IsActive: {isActive.toString()}
									</Card.Text>
									<div className="d-grid gap-2">
										<Button variant="primary" onClick={() => buy(productId)} size="lg">Buy Now</Button>
										
										<Button variant="info" size="sm" onClick={() => setProdPrice(productId.prodPrice)} > Add to Cart </Button>
										
									</div>
								</Card.Body>		
							</Card>
						</Col>
					</Row>
		</Container>
	)
}


