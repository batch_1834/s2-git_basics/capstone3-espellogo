import Banner from "../components/Banner";
import HighLights from "../components/HighLights"



export default function Home(){
	const data = {
		title: "The Gadget Store",
		content: "Your Number 1 Online Store!" ,
		destination: "/Products",
		label: "Buy Now"
	}


	return(
		<>
			<HighLights/>
			<Banner data={data}/>	        
		</>
	)
}