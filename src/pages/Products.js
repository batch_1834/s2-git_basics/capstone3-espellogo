import {useEffect, useState, useContext} from "react";
import ProductCard from "../components/ProductCard"
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";

export default function Products(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext)

	// State that will be used to store the courses retrieve from the database

	// Retrieve the courses from the database upon the initial render of the Course component.

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			setProducts(data.map(product =>{
				return( <ProductCard key={product._id} productsProp={product}/> )
			}))

		})
	},[])
	
	return(
		(user.isAdmin)
		?
			<Navigate to="/admin"/>
		:
		<>
			<h1 className="text-center">Products</h1>
			{products}
		</>
	)
}







