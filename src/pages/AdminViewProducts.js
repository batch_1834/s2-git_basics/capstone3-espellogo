import {useEffect, useState, useContext} from "react";
import ProductCard from "../components/ProductCard"
import UserContext from "../UserContext";
import {Navigate, useParams} from "react-router-dom";
import {Row, Col, Card, Button, Container} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Products(product){

	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("")
	const [prodPrice, setProdPrice] = useState(0)
	const [prodQuantity, setProdQuantity] = useState(0)
	const [isActive, setIsActive] = useState(false)
	

	// const {_id, productName, description, prodPrice, prodQuantity} = product;
	const {productId} = useParams();
	const [products, setProducts] = useState([]);

		const {user} = useContext(UserContext)

		useEffect(() =>{
			if (isActive == true) {
			       setIsActive(true);
			    }
			console.log(isActive)
			fetch(`${process.env.REACT_APP_API_URL}/products/`)			
			.then(res => res.json())
			.then(data =>{
				console.log(data);
				setProductName(data.productName);
				setDescription(data.description);
				setProdPrice(data.prodPrice);
				setProdQuantity(data.prodQuantity);
				setIsActive(data.isActive);
				

				setProducts(data.map(product =>{
					return( 
								<Container className="d-flex-row">
									<Row>
										<Col xs={5} md={3}>
											<Card className="p-2 my-2 ">
												<Card.Body className="text-center">
													<Card.Title>
														{product.productName}
													</Card.Title>
													<Card.Subtitle>
														Description: {product.description}
													</Card.Subtitle>
													<Card.Subtitle>Price:</Card.Subtitle>
													<Card.Text>						
														{product.prodPrice}
													</Card.Text>
													<Card.Text>						
														Quantity: {product.prodQuantity}
													</Card.Text>
													<Card.Text>						
														IsActive: {product.isActive.toString()}
													</Card.Text>
												</Card.Body>
											</Card>
										</Col>
									</Row>
								</Container>
								
						)
				}))

			})
		},[])

	
	return(
		
			<>
				<h1 className="text-center">Products</h1>
				{products}
			</>						
	)
}

