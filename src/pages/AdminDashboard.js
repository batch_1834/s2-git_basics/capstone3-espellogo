import {Table, Button, Container, Form, Col, Card, Row} from "react-bootstrap";
import React, {useContext, useState, useEffect} from "react";
import Products from "./Products";
import AdminViewProducts from "./AdminViewProducts"
import {Link} from "react-router-dom";
import UserContext from "../UserContext";
import {Navigate, useParams} from "react-router-dom";
import Modal from 'react-bootstrap/Modal';
import Swal from "sweetalert2";



export default function AdminDashboard(){

	// for modal
	const [ show, setShow ] = useState(false);
  	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);
  	


	// to validate the user role.
	const {user} = useContext(UserContext);
	const {productId} = useParams();

	// Create a allCourses State to contain the products from the database.
	const [allProducts, setAllProducts] = useState([]);
	const [productName, setProductName] = useState(" ");
	const [description, setDescription] = useState(" ");
	const [prodPrice, setProdPrice] = useState(0);
	const [prodQuantity, setProdQuantity] = useState(0);
	const [editProduct, setEditProduct] = useState([]);

	// "fetchData()" wherein we can invoke if their is a certain change with the product.
	const fetchData = () =>{
			fetch(`${process.env.REACT_APP_API_URL}/products/`,{
				headers:{
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setAllProducts(data.map(product =>{
					return(
							<tr key={product._id}>
								<td>{product._id}</td>
								<td>{product.productName}</td>
								<td>{product.description}</td>
								<td>{product.prodPrice}</td>
								<td>{product.prodQuantity}</td>
								<td>{product.isActive ? "Active" : "Inactive"}</td>
								<td>
									 {
										(product.isActive)
										?
										<Button variant="danger" size="sm" onClick={() => archive(product._id, product.productName)} >Archive</Button>
										:
										<>
										<Button variant="success" size="sm" onClick={() => unarchive(product._id, product.productName)} >Unarchive</Button>
										
										<Button variant="secondary" size="sm" onClick={() => editDataProduct()} >Edit</Button>
										
										</>
									}
								</td>
							
							</tr>
						)
				}))	

			})
		}

		    
		// making the product inactive
		const archive = (productId, productName) =>{
			console.log(productId);
			console.log(productName);

			fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
				method: "PATCH",
				headers:{
					"Content-type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					isActive: false
				})
			})
			.then(res => res.json())
			.then(data =>{
				console.log(data);

				if (data) {
					Swal.fire({
						title: "Acrhive Succesful!",
						icon: "success",
						text: `${productName} is now inactive.`
					})
					fetchData();
				}
				else{
					Swal.fire({
						title: "Acrhive Unsuccesful!",
						icon: "error",
						text: `Something went wrong. Please try again later!`
					})
				}

			})

		}

		// Making the product active
		const unarchive = (productId, productName) =>{
			console.log(productId);
			console.log(productName);

			fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
				method: "PATCH",
				headers:{
					"Content-type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					isActive: true
				})
			})
			.then(res => res.json())
			.then(data =>{
				console.log(data);

				if (data) {
					Swal.fire({
						title: "Unarchive Succesful!",
						icon: "success",
						text: `${productName} is now active.`
					})
					fetchData({});
				}
				else{
					Swal.fire({
						title: "Unarchive Unsuccesful!",
						icon: "error",
						text: `Something went wrong. Please try again later!`
					})
				}

			})

		}
		
		// Add product
		const product = (e) => {
			
			console.log(product)
			fetch(`${process.env.REACT_APP_API_URL}/products/prodCreate`, {
				method: "POST",
				headers:{
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					productName: productName,
					description: description,
					prodPrice: prodPrice,
					prodQuantity: prodQuantity
				})
			})
			
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if( data !== "") {

					Swal.fire({
						title: "Successfully added",
						icon: "success",
						text: "You have successfully added new product"
					});				
				}
				
				else{
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again!."
					});
				}
				
			})		
		}

		const editDataProduct = (productId) =>{
				fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,{
					method: "PUT",
					headers:{
						"Content-type": "application/json",
						"Authorization": `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						productId: productId
					})
				})
				.then(res => res.json())
				.then(data =>{
					console.log(data);
					setProductName(data.productName);
					setDescription(data.description);
					setProdPrice(data.prodPrice);
					setProdQuantity(data.prodQuantity);
					setEditProduct(data.map(product =>{
						return(
							<tr key={product._id}>
								<td>{product._id}</td>

								<td>{productName}
								<input type="text" onChange={(productId) => setProductName(productId.target.value)} />
								</td>

								<td>{description}
								<input type="text" onChange={(productId) => setDescription(productId.target.value)}/></td>
								<td>{prodPrice}
								<input type="text" onChange={(productId) => setProdPrice(productId.target.value)} />
								</td>
								<td>{prodQuantity}
								<input type="text" onChange={(productId) => setProdQuantity(productId.target.value)}/>
								</td>

								<td>	
									<Button variant="secondary" size="sm" onClick={() => editDataProduct()} >Save</Button>	
								</td>
							
							</tr>
						)

					}))				
				})

			}


		useEffect(()=>{

		fetchData({});
		
		}, [])
		
	

	return(
		(user.isAdmin)
		?
		<>
		<Container className="mt-3 mb-3 text-center p-3">
			<div>
				<h1>Admin Dashboard</h1>
				

				
				<div>
					{/*Add products*/}
					<Button variant="primary" size="lg" onClick={handleShow} >Add Products</Button>

					{/*Show Products*/}
					 <Link as={Link} to="/productsView/:productId" eventKey="/productsView/:productId" >					 	
					 	<Button variant="success" size="lg" className="mx-2">Show Products</Button>
					 </Link>
				</div>


			{/*Modal for Add Products*/}
			<Modal show={show} onHide={handleClose}>
		        <Form onSubmit ={() => product()}>
			        <Modal.Header closeButton>
			          	<Modal.Title>Add Product</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			            <Form.Group className="mb-3" controlId="name">
			              	<Form.Label>Product Name</Form.Label>
			              	<Form.Control type="text" placeholder="Input here.." value={productName} onChange={(e) => setProductName(e.target.value)} required="required"/>
			            </Form.Group>
			            <Form.Group className="mb-3" controlId="description">
			              	<Form.Label>Description</Form.Label>
			              	<Form.Control as="textarea" rows={4} placeholder="Text area here.." value={description} onChange={(e) => setDescription(e.target.value)} required="required"/>
			            </Form.Group>
			            <Form.Group className="mb-3" controlId="price">
			              	<Form.Label>Price</Form.Label>
			              	<Form.Control type="number" placeholder="Input here.." value={prodPrice.toString()} onChange={(e) => setProdPrice(e.target.value)} required="required"/>
			            </Form.Group>
			            <Form.Group className="mb-3" controlId="stocks">
			              	<Form.Label>Product Quantity</Form.Label>
			              	<Form.Control type="number" placeholder="Input here.." value={prodQuantity.toString()}  onChange={(e) => setProdQuantity(e.target.value)} required="required"/>
			            </Form.Group>
			        </Modal.Body>
			        <Modal.Footer>
			          	<Button variant="secondary" onClick={handleClose}>
			            	Close
			          	</Button>
			          	<Button variant="primary" type="submit" id="submitBtn" onClick={(e) => (e)} > Save </Button>
			        </Modal.Footer>
		        </Form>
	      	</Modal>
	      {/*Modal End*/}
			
			</div>
		<Table striped bordered hover>
				     <thead>
				       <tr>
				         <th>Product ID</th>
				         <th>Product Name</th>
				         <th>Description</th>
				         <th>Price</th>
				         <th>Quantity</th>
				         <th>Status</th>
				         <th>Action</th>
				       </tr>
				     </thead>
				     <tbody>
				       {allProducts}
				     </tbody>
		</Table>
		</Container>

		</>
		:
		<Navigate to="/products"/>

	)
}

