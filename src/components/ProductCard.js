import { useState, useEffect } from "react";
import {Link} from "react-router-dom";
import {Row, Col, Card, Button} from "react-bootstrap";



export default function ProductCard({productsProp}){

const {_id, productName, description, prodPrice, prodQuantity, isActive} = productsProp;

return(
		<Row>
			<Col xs={4} md={5} className="p-3 my-3 justify-content text-center">
				<Card >
					<Card.Body>
						<Card.Title>
							{productName}
						</Card.Title>
						<Card.Subtitle>
							Description:
						</Card.Subtitle>
						<Card.Text>
							{description}
						</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>						
							{prodPrice}
						</Card.Text>
						<Card.Text>						
							Quantity: {prodQuantity}
						</Card.Text>
						<Card.Text>						
							isActive: {isActive.toString()}
						</Card.Text>
					{/*We will be able to select a specific course through its url*/}
						<Button as={Link} to={`/products/${_id}`} variant="primary">More Details</Button>
						{/*<Button variant="primary" onClick={enroll}>Enroll</Button>
						<Button variant="danger mx-1" onClick={unEnroll}>Unenroll</Button>*/}
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}

