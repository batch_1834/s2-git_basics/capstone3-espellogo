import {div} from "react-bootstrap";
import Carousel from 'react-bootstrap/Carousel';
import Css from '../App.css'


export default function HighLights(){
	return(
		<div className="mt-2 p-3 bg-black">
			<Carousel fade >
			      <Carousel.Item>
			        <img
			          className="d-block"
			          src="https://fdn.gsmarena.com/imgroot/news/20/06/huawei-matebook-x-pro-2020-review/-727w3/gsmarena_055.jpg"
			          alt="First slide" width="100%" height="600"
			        />
			        <Carousel.Caption>
			          <h3>HUAWEI</h3>
			          <p>High-quality touchscreen displays, good keyboards and commendable build quality..</p>
			        </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item>
			        <img
			          className="d-block"
			          src="https://wallpaperaccess.com/full/2886094.jpg"
			          alt="Second slide" width="100%" height="600"
			        />

			        <Carousel.Caption>
			          <h3>ASUS</h3>
			          <p>Asus did incredibly well in best and worst laptop brands battle.</p>
			        </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item>
			        <img
			          className="d-block"
			          src="https://images.unsplash.com/photo-1558864559-ed673ba3610b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fGRlbGwlMjBsYXB0b3B8ZW58MHx8MHx8&w=1000&q=80"
			          alt="Third slide" width="100%" height="600"
			        />

			        <Carousel.Caption>
			          <h3>Dell</h3>
			          <p>
			            Dell makes some of the best all-around laptops and 2-in-1 devices on the market.
			          </p>
			        </Carousel.Caption>
			      </Carousel.Item>
			    </Carousel>
		</div>
	)
}