// import Navbar from "react-bootstrap/Navbar";
import {Link} from "react-router-dom";
import {useState, useContext} from "react";
import UserContext from "../UserContext"



import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap"


export default function AppNavbar() {

	const {user} = useContext(UserContext);

	// State hook to store the information stored in the login page;
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user)

	return (
		<Navbar bg="info" expand="lg">
		      <Container>
		        <Navbar.Brand as={Link} to="/"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShqKrNKf1-SDE15mqGrDWEzCHC7xby7tT0pQ&usqp=CAU" alt="" width="100%" height="30"  /></Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
			        <Navbar.Collapse id="basic-navbar-nav">
			    {/*className is use instead of "class", to specify a CSS classes */}
				          <Nav className="ms-auto" defaultActiveKey="/">
				            <Nav.Link as={Link} to="/">Home</Nav.Link>

				            {
				            	user.isAdmin
				            	?
				            	<Nav.Link as={Link} to="/admin" eventKey="/admin" >Admin Dashboard</Nav.Link>
				            	:
				            	<>
				            	<Nav.Link as={Link} to="/products" eventKey="/products" >Products</Nav.Link>
				            	{/*<Nav.Link as={Link} to="/cart" eventKey="/cart" >My-Cart</Nav.Link>*/}
				            	</> 	
				            }
				            
				            {
				            	(user.id !== null)
				            	?
				  
				            		<Nav.Link as={Link} to="/logout" eventKey="/logout" >Logout</Nav.Link>
				            	:
					            	<>					            		
						            	<Nav.Link as={Link} to="/register" eventKey="/register" >Register</Nav.Link>
						            	<Nav.Link as={Link} to="/login" eventKey="/login" >Login</Nav.Link> 
						            </>
				            	
				            }
				            

				          </Nav>
			        </Navbar.Collapse>
		      </Container>
		    </Navbar>
	)
}